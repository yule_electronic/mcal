/**
*   @file    Mcu_SCG.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - System Clock Generator functions implementation.
*   @details Specific functions for SCG configuration and control.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif




/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_SCG.h"
#include "Reg_eSys_SCG.h"

#if (MCU_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /* (MCU_DEV_ERROR_DETECT == STD_ON) */

#if (MCU_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF)
#include "Dem.h"
#endif

#ifndef USER_MODE_REG_PROT_ENABLED
#define USER_MODE_REG_PROT_ENABLED ( MCU_USER_MODE_REG_PROT_ENABLED )
#endif

#include "SilRegMacros.h"

/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/

#if (MCU_INIT_CLOCK == STD_ON)

/**
* @brief            This function will configure the system clock for all modes
*/
FUNC( void, MCU_CODE) Mcu_SCG_SystemClockInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    REG_WRITE32(SCG_RCCR_ADDR32, pConfigPtr->u32RunClockControlConfiguration);
    REG_WRITE32(SCG_VCCR_ADDR32, pConfigPtr->u32VLPRClockControlConfiguration);
#ifdef MCU_HSRUN_MODE_NOT_SUPPORT
#if (MCU_HSRUN_MODE_NOT_SUPPORT == STD_ON)
    /* do nothing */
#else
    REG_WRITE32(SCG_HCCR_ADDR32, pConfigPtr->u32HSRUNClockControlConfiguration);
#endif
#else
    REG_WRITE32(SCG_HCCR_ADDR32, pConfigPtr->u32HSRUNClockControlConfiguration);
#endif
    REG_WRITE32(SCG_CLKOUTCNFG_ADDR32, pConfigPtr->u32ClockOutConfiguration);
}
/**
* @brief            This function will select SIRC as system clock
*/
FUNC( void, MCU_CODE) Mcu_SCG_DropSystemClockToSirc(VAR( void, AUTOMATIC))
{
    VAR(uint32, AUTOMATIC) u32TimeOut = 0U;
    VAR(uint32, AUTOMATIC) u32RegValue = 0U;
    
    /* Disable SIRC so the rest of the register can be configured. */
    REG_BIT_CLEAR32(SCG_SIRCCSR_ADDR32, SCG_SIRCCSR_SIRCEN_MASK32);
    
    /* write some default values for SIRC registers */
    REG_WRITE32(SCG_SIRCCSR_ADDR32,SCG_SIRCCSR_LK_UNLOCK_U32);
    REG_WRITE32(SCG_SIRCCFG_ADDR32,SCG_SIRCCFG_HIGH_RANGE_CLOCK_U32);
    REG_WRITE32(SCG_SIRCDIV_ADDR32,SCG_SIRCDIV_SIRCDIV2_U32(1U) |  SCG_SIRCDIV_SIRCDIV1_U32(1U));
    REG_BIT_SET32(SCG_SIRCCSR_ADDR32, SCG_SIRCCSR_SIRCEN_MASK32);
    /*Wait for Valid bit done */
    u32TimeOut = MCU_TIMEOUT_LOOPS;
    do
    {
        u32TimeOut--;
        u32RegValue = REG_READ32(SCG_SIRCCSR_ADDR32) & SCG_SIRCCSR_SIRCVLD_MASK32;
    }
    while ((SCG_SIRCCSR_SIRCVLD_MASK32 != u32RegValue) && ((uint32)0x00U < u32TimeOut));
    
    if ( (uint32)0x0U == u32TimeOut)
    {
    }
    /* Set SIRC as system clock */
    REG_RMW32(SCG_RCCR_ADDR32, SCG_RCCR_SCS_MASK32, SCG_SCS_SIRC_U32);
    REG_RMW32(SCG_VCCR_ADDR32, SCG_VCCR_SCS_MASK32, SCG_SCS_SIRC_U32);
#ifdef MCU_HSRUN_MODE_NOT_SUPPORT
#if (MCU_HSRUN_MODE_NOT_SUPPORT == STD_ON)
    /* do nothing */
#else
    REG_RMW32(SCG_HCCR_ADDR32, SCG_HCCR_SCS_MASK32, SCG_SCS_SIRC_U32);
#endif
#else
    REG_RMW32(SCG_HCCR_ADDR32, SCG_HCCR_SCS_MASK32, SCG_SCS_SIRC_U32);
#endif
}

/**
* @brief            This function will configure the System OSC
*
* @details          This function will configure the SOSC control, divider and trim registers
*
*/
FUNC( void, MCU_CODE) Mcu_SCG_SoscInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    VAR(uint32, AUTOMATIC) u32Counter = 0U;
    VAR(uint32, AUTOMATIC) u32TimeOut = 0U;
    VAR(uint32, AUTOMATIC) u32RegValue = 0U;
    
    /* Disable SOSC so the rest of the register can be configured. */
    REG_BIT_CLEAR32(SCG_SOSCCSR_ADDR32, SCG_SOSCCSR_SOSCEN_MASK32);

    /* The last register configured is SOSCCSR so the SOSC will be enabled or disabled, depending on the user configuration. */
    for ( u32Counter = (uint32)0x00U; u32Counter < MCU_NUMBER_OF_SOSC_REGISTERS_U32; u32Counter++)
    {
        REG_WRITE32((*pConfigPtr->apSoscClockConfig)[u32Counter].u32RegisterAddr, (*pConfigPtr->apSoscClockConfig)[u32Counter].u32RegisterData);
    }
    
    /*Wait for Valid bit done */
    u32TimeOut = MCU_TIMEOUT_LOOPS;
    do
    {
        u32TimeOut--;
        u32RegValue = REG_READ32(SCG_SOSCCSR_ADDR32) & SCG_SOSCCSR_SOSCVLD_MASK32;
    }
    while ((SCG_SOSCCSR_SOSCVLD_MASK32 != u32RegValue) && ((uint32)0x00U < u32TimeOut));
    
    if ( (uint32)0x0U == u32TimeOut)
    {
    }
}

/**
* @brief            This function will configure the Slow IRC
*
* @details          This function will configure the SIRC control, divider and trim registers
*
*/
FUNC( void, MCU_CODE) Mcu_SCG_SircInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    VAR(uint32, AUTOMATIC) u32Counter;
    VAR(uint32, AUTOMATIC) u32TimeOut = 0U;
    VAR(uint32, AUTOMATIC) u32RegValue = 0U;
    
    /* Disable SIRC so the rest of the register can be configured. */
    REG_BIT_CLEAR32(SCG_SIRCCSR_ADDR32, SCG_SIRCCSR_SIRCEN_MASK32);

    /* The last register configured is SIRCCSR so the SIRC will be enabled or disabled, depending on the user configuration. */
    for ( u32Counter = (uint32)0x00U; u32Counter < MCU_NUMBER_OF_SIRC_REGISTERS_U32; u32Counter++)
    {
        REG_WRITE32((*pConfigPtr->apSircClockConfig)[u32Counter].u32RegisterAddr, (*pConfigPtr->apSircClockConfig)[u32Counter].u32RegisterData);
    }
    /*Wait for Valid bit done */
    u32TimeOut = MCU_TIMEOUT_LOOPS;
    do
    {
        u32TimeOut--;
        u32RegValue = REG_READ32(SCG_SIRCCSR_ADDR32) & SCG_SIRCCSR_SIRCVLD_MASK32;
    }
    while ((SCG_SIRCCSR_SIRCVLD_MASK32 != u32RegValue) && ((uint32)0x00U < u32TimeOut));
    
    if ( (uint32)0x0U == u32TimeOut)
    {
    }
}

/**
* @brief            This function will configure the Fast IRC
*
* @details          This function will configure the FIRC control, divider and trim registers
*
*/
FUNC( void, MCU_CODE) Mcu_SCG_FircInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    VAR(uint32, AUTOMATIC) u32Counter;
    VAR(uint32, AUTOMATIC) u32TimeOut = MCU_TIMEOUT_LOOPS;
    VAR(uint32, AUTOMATIC) u32RegValue = 0U;
    
    /* Disable FIRC so the rest of the register can be configured. */
    REG_BIT_CLEAR32(SCG_FIRCCSR_ADDR32, SCG_FIRCCSR_FIRCEN_MASK32);

    /* The last register configured is FIRCCSR so the FIRC will be enabled or disabled, depending on the user configuration. */
    for ( u32Counter = (uint32)0x00U; u32Counter < MCU_NUMBER_OF_FIRC_REGISTERS_U32; u32Counter++)
    {
        REG_WRITE32((*pConfigPtr->apFircClockConfig)[u32Counter].u32RegisterAddr, (*pConfigPtr->apFircClockConfig)[u32Counter].u32RegisterData);
    }
    
    /* Check whether the mode FIRC is enable or not. */
    if((REG_READ32(SCG_FIRCCSR_ADDR32) & SCG_FIRCCSR_FIRCEN_MASK32) == SCG_FIRCCSR_FIRCEN_MASK32)
    {
        /* wait for valid bit done. */
        do
        {
            u32TimeOut--;
            u32RegValue = REG_READ32(SCG_FIRCCSR_ADDR32) & SCG_FIRCCSR_FIRCVLD_MASK32; 
        }
        while ((SCG_FIRCCSR_FIRCVLD_MASK32 != u32RegValue) && ((uint32)0x00U < u32TimeOut));
    }
    
    if ( (uint32)0x0U == u32TimeOut)
    {
    }
}

#if (MCU_NO_PLL == STD_OFF)
/**
* @brief            This function will configure the System PLL
*
* @details          This function will configure the SPLL control, divider and configuration registers
*
*/
FUNC( void, MCU_CODE) Mcu_SCG_SpllInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    VAR(uint32, AUTOMATIC) u32Counter;

    /* Disable SPLL so the rest of the register can be configured. */
    REG_BIT_CLEAR32(SCG_SPLLCSR_ADDR32, SCG_SPLLCSR_SPLLEN_MASK32);

    /* The last register configured is SCG_SPLLCSR so the SPLL will be enabled or disabled, depending on the user configuration. */
    for ( u32Counter = (uint32)0x00U; u32Counter < MCU_NUMBER_OF_SPLL_REGISTERS_U32; u32Counter++)
    {
        REG_WRITE32((*pConfigPtr->apSpllClockConfig)[u32Counter].u32RegisterAddr, (*pConfigPtr->apSpllClockConfig)[u32Counter].u32RegisterData);
    }
}
#endif  /* MCU_NO_PLL */

#endif /* (MCU_INIT_CLOCK == STD_ON) */

#if (MCU_NO_PLL == STD_OFF)
/**
* @brief            This function gets the status of the PLL
*
* @details          This function will return the PLL status
*
*/
FUNC( Mcu_SCG_PllStatusType, MCU_CODE) Mcu_SCG_GetPLLStatus(VAR( void, AUTOMATIC))
{
    VAR(Mcu_SCG_PllStatusType, AUTOMATIC) ePllStatus = (Mcu_SCG_PllStatusType)MCU_SPLL_STATUS_UNDEFINED;
    VAR(uint32, AUTOMATIC) u32PllStatus;

    u32PllStatus = REG_READ32( SCG_SPLLCSR_ADDR32 ) & SCG_SPLLCSR_SPLLVLD_MASK32;
    /* The last register configured is SCG_SPLLCSR so the SPLL will be enabled or disabled, depending on the user configuration. */
    if ( SCG_SPLLCSR_SPLLVLD_MASK32 == u32PllStatus)
    {
        ePllStatus = MCU_SPLL_LOCKED;
    }
    else
    {
        ePllStatus = MCU_SPLL_UNLOCKED;
    }
    
    return ePllStatus;
}
#endif

/**
* @brief            This function will disable all clock monitors
* @violates @ref Mcu_SCG_c_REF_6 Violates MISRA 2004 Required Rule 8.10, global declaration of function
*/
FUNC( void, MCU_CODE) Mcu_SCG_DisableClockMonitors(VAR( void, AUTOMATIC))
{
    REG_RMW32(SCG_SOSCCSR_ADDR32,SCG_SOSCCSR_SOSCCM_MASK32,SCG_SOSCCSR_SOSCCM_DIS_U32);
#if (MCU_NO_PLL == STD_OFF)
    REG_RMW32(SCG_SPLLCSR_ADDR32,SCG_SPLLCSR_SPLLCM_MASK32,SCG_SPLLCSR_SPLLCM_DIS_U32);    
#endif  /* MCU_NO_PLL */
}

#if (MCU_NO_PLL == STD_OFF)
/**
* @brief            This function will disable PLL clock
*/
FUNC( void, MCU_CODE) Mcu_SCG_DisableSpllClock(VAR( void, AUTOMATIC))
{
    REG_RMW32(SCG_SPLLCSR_ADDR32,SCG_SPLLCSR_SPLLEN_MASK32,SCG_SPLLCSR_SPLL_DISABLE_U32);    
}
#endif  /* MCU_NO_PLL */

#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)
/**
* @brief            This function will configurable SIRC as system clock.
*/
FUNC( void, MCU_CODE) Mcu_SCG_DropSysClkToSircInRunMode(VAR( void, AUTOMATIC))
{
    /* Set SIRC as system clock in Run mode */
    REG_RMW32(SCG_RCCR_ADDR32, SCG_RCCR_SCS_MASK32, SCG_SCS_SIRC_U32);
}

/**
* @brief            This function will disable FIRC clock.
*/
FUNC( void, MCU_CODE) Mcu_SCG_DisableFircClock(VAR( void, AUTOMATIC))
{
    /* Disable FIRC clock */
    REG_BIT_CLEAR32(SCG_FIRCCSR_ADDR32, SCG_FIRCCSR_FIRCEN_MASK32);
}

/**
* @brief            This function will disable SOSC clock.
*/
FUNC( void, MCU_CODE) Mcu_SCG_DisableSoscClock(VAR( void, AUTOMATIC))
{
    /* Disable SOSC clock */
    REG_BIT_CLEAR32(SCG_SOSCCSR_ADDR32, SCG_SOSCCSR_SOSCEN_MASK32);
}
#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
