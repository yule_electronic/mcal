/**
*   @file    Mcu_RCM.c
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Reset Control Module functions implementation.
*   @details Specific functions for RCM configuration and control.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifdef __cplusplus
extern "C"
{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
* 1) system and project includes
* 2) needed interfaces from external units
* 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_RCM.h"
#include "Reg_eSys_RCM.h"

#if (defined(MCU_RESET_ALTERNATE_ISR_USED) && (MCU_RESET_ALTERNATE_ISR_USED == STD_ON))
#ifdef MCU_ERROR_ISR_NOTIFICATION
#include "Mcu.h"
#endif
#endif

#if (MCU_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /* (MCU_DEV_ERROR_DETECT == STD_ON) */

#ifndef USER_MODE_REG_PROT_ENABLED
#define USER_MODE_REG_PROT_ENABLED ( MCU_USER_MODE_REG_PROT_ENABLED )
#endif

#include "SilRegMacros.h"


/*==================================================================================================
                                       LOCAL VARIABLES
==================================================================================================*/
#define MCU_START_SEC_VAR_INIT_32
#include "Mcu_MemMap.h"

static VAR(uint32, MCU_VAR) Mcu_u32ResetStatus = 0x00U;

#define MCU_STOP_SEC_VAR_INIT_32
#include "Mcu_MemMap.h"


/*==================================================================================================
                                       LOCAL FUNCTIONS
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


/*==================================================================================================
                                       GLOBAL FUNCTIONS
==================================================================================================*/
#if (defined(MCU_DISABLE_RCM_INIT) && (STD_OFF == MCU_DISABLE_RCM_INIT))
/**
* @brief            This function initializes the Reset parameters.
* @details          Conigures the threshold reset value.
*                   Called by:
*                       - Mcu_IPW_Init() from IPW.
*
* @param[in]        pConfigPtr   Pointer to the RCM configuration structure.
*
* @return           void
*
*/
FUNC( void, MCU_CODE) Mcu_RCM_ResetInit(P2CONST( Mcu_RCM_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr)
{
    /** @violates @ref Mcu_RCM_c_REF_4 Conversion from int to pointer */
    /** @violates @ref Mcu_RCM_c_REF_5 The cast is used to access memory mapped registers.*/
    REG_WRITE32(RCM_RPC_ADDR32, (uint32)(pConfigPtr->u32ResetPinControlConfig & RCM_RPC_RWBITS_MASK32));
    
    /** @violates @ref Mcu_RCM_c_REF_4 Conversion from int to pointer */
    /** @violates @ref Mcu_RCM_c_REF_5 The cast is used to access memory mapped registers.*/
    REG_WRITE32(RCM_SRIE_ADDR32, (uint32)(pConfigPtr->u32ResetInterruptEnableConfig & RCM_SRIE_RWBITS_MASK32));
}
#endif /* #if (defined(MCU_DISABLE_RCM_INIT) && (STD_OFF == MCU_DISABLE_RCM_INIT)) */

/**
* @brief            This function returns the Reset reason.
* @details          This routine returns the Reset reason that is read from the hardware.
*                   Called by:
*                       - Mcu_IPW_GetResetReason() from IPW.
*
* @return           Reason of the Reset event.
*
* @note             The User should ensure that the reset reason is cleared once it has been read
*                   out to avoid multiple reset reasons. The function Mcu_GetResetReason shall
*                   return MCU_RESET_UNDEFINED if this function is called prior to calling of the
*                   function Mcu_Init, and if supported by the hardware.
*/
FUNC( Mcu_ResetType, MCU_CODE) Mcu_RCM_GetResetReason( VAR( void, AUTOMATIC))
{
    /* Code for the Reset event returned by this function. */
    VAR( Mcu_ResetType, AUTOMATIC) eResetReason = MCU_NO_RESET_REASON;
    /* Temporary variable for RCM_RSR register value. */
    VAR( uint32, AUTOMATIC) u32RegValue = 0U;
    VAR( uint32, AUTOMATIC) u32ActiveValue;
    VAR( uint32, AUTOMATIC) u32Index;
    VAR( uint32, AUTOMATIC) u32DynamicMask;
    VAR( uint32, AUTOMATIC) u32Position = (uint32)0x00U;
    VAR( uint32, AUTOMATIC) u32NumberOfFlags = 0U;
    
    /* Check reset reasons from SSRS Status Register. */
    u32RegValue = (uint32)REG_READ32(RCM_SSRS_ADDR32) & RCM_SSRS_RWBITS_MASK32;
    
    /* Store the content of RSR */
    if (0x00U != u32RegValue)
    {
        /* Clear the flags if any flag is set */
        REG_WRITE32(RCM_SSRS_ADDR32, (uint32)(u32RegValue & RCM_SSRS_RWBITS_MASK32));
        
        Mcu_u32ResetStatus = u32RegValue;
    }
    u32ActiveValue = Mcu_u32ResetStatus;
    
    if(RCM_SSRS_SPOR_SLVD_MASK32 == (u32ActiveValue & RCM_SSRS_RWBITS_MASK32))
    {
        eResetReason = MCU_POWER_ON_RESET;
    }
    else
    {
        for (u32Index = 0x00U; u32Index < 0x20U; u32Index++)
        {
            u32DynamicMask = ((uint32)0x80000000U >> u32Index);
            if ((uint32)0x00U != (u32DynamicMask & RCM_SSRS_RWBITS_MASK32))
            {
                if ((uint32)0x00U != (u32DynamicMask & u32ActiveValue))
                {
                    eResetReason = (Mcu_ResetType)u32Position;
                    u32NumberOfFlags++;
                    /* MCU_MULTIPLE_RESET_REASON returned if more than one reset reason in this case use function Mcu_GetRawValue to determine. */
                    if (u32NumberOfFlags >= (uint32)2)
                    {
                        eResetReason = MCU_MULTIPLE_RESET_REASON;
                        break;
                    }
                }
                u32Position++;
            }
        }
    }
    return (Mcu_ResetType)eResetReason;
}

/**
* @brief            This function returns the Raw Reset value.
* @details          This routine returns the Raw Reset value that is read from the hardware.
*                   Called by:
*                       - Mcu_IPW_GetResetRawValue() from IPW.
*
* @return           Register value with the Reset status.
* @retval           uint32   Code of the Raw reset value.
*
* @note             The User should ensure that the reset reason is cleared once it has been read
*                   out to avoid multiple reset reasons.
*
*/
FUNC( Mcu_RawResetType, MCU_CODE) Mcu_RCM_GetResetRawValue( VAR( void, AUTOMATIC))
{
    VAR( uint32, AUTOMATIC) u32RawReset;
    VAR( uint32, AUTOMATIC) u32RegValue;

    u32RegValue = REG_READ32(RCM_SSRS_ADDR32) & RCM_SSRS_RWBITS_MASK32;

    if ((uint32)0x00U != u32RegValue)
    {
        /* Clear the flags if any flag is set */
        REG_WRITE32(RCM_SSRS_ADDR32, (uint32)(u32RegValue & RCM_SSRS_RWBITS_MASK32));
        
        Mcu_u32ResetStatus = u32RegValue;
    }
    
    u32RawReset = Mcu_u32ResetStatus;

    return (Mcu_RawResetType)u32RawReset;
}

#ifdef MCU_ERROR_ISR_NOTIFICATION
#if (defined(MCU_RESET_ALTERNATE_ISR_USED) && (MCU_RESET_ALTERNATE_ISR_USED == STD_ON))
/**
* @brief            This function handles the Reset Alternate Event Interrupts.
* @details          Only the following reset events support interrupt reaction:
*                       - SACKERR (Stop Acknowledge Error)
*                       - MDM_AP (MDP-AP System Reset Request)
*                       - SW (Software Reset)
*                       - LOCKUP (Core Lockup)
*                       - JTAG (JTAG Generated Reset)
*                       - GIE (Global Interrupt Enable)
*                       - PIN (External Reset Pin)
*                       - WDOG (Watchdog)
*                       - CMU_LOC (CMU Loss-Of-Clock)
*                       - LOL (Loss-Of-Lock)
*                       - LOC (Loss-Of-Clock)
*
* @return           void
* @implements Mcu_RCM_ResetAltInt_Activity
*/
FUNC(void, MCU_CODE) Mcu_RCM_ResetAltInt(void)
{
    VAR(uint32, AUTOMATIC) u32ResetInterruptEnable = 0U;

    u32ResetInterruptEnable = REG_READ32(RCM_SRIE_ADDR32) & RCM_SRIE_IRQ_BITS_MASK32;

    if (NULL_PTR != Mcu_pConfigPtr)
    {
        if ( (uint32)0U != u32ResetInterruptEnable )
        {
            MCU_ERROR_ISR_NOTIFICATION(MCU_E_ISR_RESET_ALT_FAILURE);
        }
    }
}
#endif /* ( MCU_RESET_ALTERNATE_ISR_USED == STD_ON ) */
#endif /* (MCU_ERROR_ISR_NOTIFICATION) */

#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)
/**
* @brief    This function returns the value of RCM_SRIE masked configured System Reset Interrupt
*/
FUNC( uint32, MCU_CODE) Mcu_RCM_GetCurrentSystemResetIsrSettings( VAR( void, AUTOMATIC))
{
    VAR( uint32, AUTOMATIC) u32SystemResetIsrStatus;
    /* get RCM_SRIE and mask agains what i need */
    u32SystemResetIsrStatus = REG_READ32(RCM_SRIE_ADDR32);
    u32SystemResetIsrStatus &= RCM_SRIE_RWBITS_MASK32;

    return ((uint32)u32SystemResetIsrStatus);
}

/**
* @brief    This function Configure all reset sources to be 'Reset' (not as Interrupt) via RCM_SRIE
*/
FUNC( void, MCU_CODE) Mcu_RCM_SystemResetIsrConfig( VAR( void, AUTOMATIC))
{
    REG_WRITE32(RCM_SRIE_ADDR32, (uint32)RCM_SRIE_RESET_MASK32);
}

/**
* @brief    This function Restore System Reset Interrupt Config
*/
FUNC(void, MCU_CODE) Mcu_RCM_RestoreSystemResetIsrConfig(VAR(uint32, AUTOMATIC) u32SystemResetIsrConfig)
{
    REG_WRITE32(RCM_SRIE_ADDR32, (uint32)(u32SystemResetIsrConfig & RCM_SRIE_RWBITS_MASK32));
}
#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

/** @} */
