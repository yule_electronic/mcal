/**
*   @file    Mcu.h
*   @implements Mcu.h_Artifact
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Driver external interface.
*   @details Contains all the public functions and data types that are used by the higher layer.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_H
#define MCU_H

#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
/**
* @brief            Import all data types from lower layers that should be exported.
*                   Mcu.h shall include Mcu_Cfg.h for the API pre-compiler switches.
*
*/
#include "Mcu_Cfg.h"
#include "Mcu_EnvCfg.h"
#include "Mcu_IPW_Types.h"
#include "Mcal.h"

/*==================================================================================================
                                           CONSTANTS
==================================================================================================*/


/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/

/*==================================================================================================
                                             ENUMS
==================================================================================================*/


/*==================================================================================================
                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/
/**
* @brief          Initialization data for the MCU driver.
* @details        A pointer to such a structure is provided to the MCU initialization routines for
*                 configuration.
* @implements     Mcu_ConfigType_struct
*/
typedef struct
{
#if (MCU_DISABLE_DEM_REPORT_ERROR_STATUS == STD_OFF)
    /**< @brief DEM error reporting configuration. */
    P2CONST( Mcu_DemConfigType, MCU_VAR, MCU_APPL_CONST) Mcu_pDemConfig;
#endif
    /**< @brief Total number of RAM sections. */
    VAR( Mcu_RamSectionType, MCU_VAR) Mcu_NoRamConfigs;
    /**< @brief Total number of  MCU modes. */
    VAR( Mcu_ModeType, MCU_VAR) Mcu_NoModeConfigs;
#if (MCU_INIT_CLOCK == STD_ON)
    /**< @brief Total number of MCU clock configurations. */
    VAR( Mcu_ClockType, MCU_VAR) Mcu_NoClkConfigs;
#endif /* (MCU_INIT_CLOCK == STD_ON) */
    /**< @brief Power Modes data configuration. */
    CONST( Mcu_ModeConfigType, MCU_CONST) (*Mcu_apModeConfig)[MCU_MAX_MODECONFIGS];
#if (MCU_LPU_SUPPORT == STD_ON)
    /**< @brief Low Power Modes data configuration. */
    CONST( Mcu_LPU_LowPowerModeConfigType, MCU_CONST) (*Mcu_apLowPowerModeConfig)[MCU_MAX_MODECONFIGS];
#endif
#if (MCU_INIT_CLOCK == STD_ON)
    /**< @brief Clock data configuration. */
    CONST( Mcu_ClockConfigType, MCU_CONST) (*Mcu_apClockConfig)[MCU_MAX_CLKCONFIGS];
#endif /* (MCU_INIT_CLOCK == STD_ON) */
    /**< @brief IPs data generic configuration. */
    P2CONST( Mcu_HwIPsConfigType, MCU_VAR, MCU_APPL_CONST) Mcu_pHwIPsConfig;

} Mcu_ConfigType;

/*==================================================================================================
                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/
#define MCU_START_SEC_VAR_INIT_UNSPECIFIED
#include "Mcu_MemMap.h"
/**
* @brief            Local copy of the pointer to the configuration data
*/
extern P2CONST( Mcu_ConfigType, MCU_VAR, MCU_APPL_CONST) Mcu_pConfigPtr;

#define MCU_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "Mcu_MemMap.h"

/*==================================================================================================
                                     FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

FUNC (void, MCU_CODE) Mcu_Init( P2CONST(Mcu_ConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);

#if (MCU_INIT_CLOCK == STD_ON)
FUNC (Std_ReturnType, MCU_CODE) Mcu_InitClock( VAR(Mcu_ClockType, AUTOMATIC) ClockSetting);
#endif 

FUNC (void, MCU_CODE) Mcu_SetMode( VAR(Mcu_ModeType, AUTOMATIC) McuMode);

#if (MCU_INIT_CLOCK == STD_ON)
#if (MCU_NO_PLL == STD_OFF)
FUNC( Std_ReturnType, MCU_CODE) Mcu_DistributePllClock( VAR( void, AUTOMATIC));
#endif
#endif

FUNC( Mcu_PllStatusType, MCU_CODE) Mcu_GetPllStatus( VAR( void, AUTOMATIC));
FUNC( Mcu_ResetType, MCU_CODE) Mcu_GetResetReason( VAR( void, AUTOMATIC));
FUNC( Mcu_RawResetType, MCU_CODE) Mcu_GetResetRawValue( VAR( void, AUTOMATIC));

#if MCU_PERFORM_RESET_API == STD_ON
FUNC( void, MCU_CODE) Mcu_PerformReset( VAR( void, AUTOMATIC));
#endif


#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

#endif /* MCU_H */

/** @} */

