/**
*   @file    Mcu_SCG.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Function prototypes.
*   @details Interface available for IPW layer only.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_SCG_H
#define MCU_SCG_H


#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_SCG_Types.h"
#include "Mcu_EnvCfg.h"

/*==================================================================================================
*                                    FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"


#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_DropSysClkToSircInRunMode() \
do\
{ \
        Mcal_Trusted_Call(Mcu_SCG_DropSysClkToSircInRunMode); \
}\
while(0)
#else
#define  Call_Mcu_SCG_DropSysClkToSircInRunMode() \
do\
{ \
        Mcu_SCG_DropSysClkToSircInRunMode(); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_DisableFircClock() \
do\
{ \
        Mcal_Trusted_Call(Mcu_SCG_DisableFircClock); \
}\
while(0)
#else
#define  Call_Mcu_SCG_DisableFircClock() \
do\
{ \
        Mcu_SCG_DisableFircClock(); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_DisableSoscClock() \
do\
{ \
        Mcal_Trusted_Call(Mcu_SCG_DisableSoscClock); \
}\
while(0)
#else
#define  Call_Mcu_SCG_DisableSoscClock() \
do\
{ \
        Mcu_SCG_DisableSoscClock(); \
}\
while(0)
#endif
#endif

#endif /* (MCU_ENTER_LOW_POWER_MODE == STD_ON) */

#if (MCU_INIT_CLOCK == STD_ON)

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_DropSystemClockToSirc() \
do\
{ \
        Mcal_Trusted_Call(Mcu_SCG_DropSystemClockToSirc); \
}\
while(0)
#else
#define  Call_Mcu_SCG_DropSystemClockToSirc() \
do\
{ \
        Mcu_SCG_DropSystemClockToSirc(); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_SircInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SCG_SircInit,(SCG_pClockConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SCG_SircInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcu_SCG_SircInit(SCG_pClockConfigPtr); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_FircInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SCG_FircInit,(SCG_pClockConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SCG_FircInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcu_SCG_FircInit(SCG_pClockConfigPtr); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_SoscInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SCG_SoscInit,(SCG_pClockConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SCG_SoscInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcu_SCG_SoscInit(SCG_pClockConfigPtr); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_SpllInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SCG_SpllInit,(SCG_pClockConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SCG_SpllInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcu_SCG_SpllInit(SCG_pClockConfigPtr); \
}\
while(0)
#endif
#endif

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_SystemClockInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcal_Trusted_Call1param(Mcu_SCG_SystemClockInit,(SCG_pClockConfigPtr)); \
}\
while(0)
#else
#define  Call_Mcu_SCG_SystemClockInit(SCG_pClockConfigPtr) \
do\
{ \
        Mcu_SCG_SystemClockInit(SCG_pClockConfigPtr); \
}\
while(0)
#endif
#endif

#endif /* (MCU_INIT_CLOCK == STD_ON) */

#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_DisableClockMonitors() \
do\
{ \
        Mcal_Trusted_Call(Mcu_SCG_DisableClockMonitors); \
}\
while(0)
#else
#define  Call_Mcu_SCG_DisableClockMonitors() \
do\
{ \
        Mcu_SCG_DisableClockMonitors(); \
}\
while(0)
#endif
#endif

#if (MCU_NO_PLL == STD_OFF)
#ifdef MCU_ENABLE_USER_MODE_SUPPORT
#if (STD_ON == MCU_ENABLE_USER_MODE_SUPPORT)
#define  Call_Mcu_SCG_DisableSpllClock() \
do\
{ \
        Mcal_Trusted_Call(Mcu_SCG_DisableSpllClock); \
}\
while(0)
#else
#define  Call_Mcu_SCG_DisableSpllClock() \
do\
{ \
        Mcu_SCG_DisableSpllClock(); \
}\
while(0)
#endif  /* MCU_NO_PLL */
#endif
#endif


#if (MCU_INIT_CLOCK == STD_ON)

FUNC( void, MCU_CODE) Mcu_SCG_SystemClockInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
FUNC( void, MCU_CODE) Mcu_SCG_SoscInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
FUNC( void, MCU_CODE) Mcu_SCG_FircInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
FUNC( void, MCU_CODE) Mcu_SCG_SircInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
#if (MCU_NO_PLL == STD_OFF)
FUNC( void, MCU_CODE) Mcu_SCG_SpllInit(P2CONST(Mcu_SCG_ClockConfigType, AUTOMATIC, MCU_APPL_CONST) pConfigPtr);
#endif  /* MCU_NO_PLL */
FUNC( void, MCU_CODE) Mcu_SCG_DropSystemClockToSirc(VAR( void, AUTOMATIC));
#endif

#if (MCU_NO_PLL == STD_OFF)
FUNC( Mcu_SCG_PllStatusType, MCU_CODE) Mcu_SCG_GetPLLStatus(VAR( void, AUTOMATIC));
FUNC( void, MCU_CODE) Mcu_SCG_DisableSpllClock(VAR( void, AUTOMATIC));
#endif

FUNC( void, MCU_CODE) Mcu_SCG_DisableClockMonitors(VAR( void, AUTOMATIC));

#if (MCU_ENTER_LOW_POWER_MODE == STD_ON)
FUNC( void, MCU_CODE) Mcu_SCG_DropSysClkToSircInRunMode(VAR( void, AUTOMATIC));
FUNC( void, MCU_CODE) Mcu_SCG_DisableFircClock(VAR( void, AUTOMATIC));
FUNC( void, MCU_CODE) Mcu_SCG_DisableSoscClock(VAR( void, AUTOMATIC));
#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"


#ifdef __cplusplus
}
#endif

#endif /* MCU_SCG_H */

/** @} */
