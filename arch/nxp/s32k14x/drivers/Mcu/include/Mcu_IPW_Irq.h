/**
*   @file    Mcu_IPW_Irq.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Midle layer interface for interrupts.
*   @details File contains function prototypes used by IPV ISRs only.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_IPW_IRQ_H
#define MCU_IPW_IRQ_H

#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/

/*==================================================================================================
                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
#define MCU_IPW_IRQ_VENDOR_ID                       43
#define MCU_IPW_IRQ_AR_RELEASE_MAJOR_VERSION        4
#define MCU_IPW_IRQ_AR_RELEASE_MINOR_VERSION        3
#define MCU_IPW_IRQ_AR_RELEASE_REVISION_VERSION     1
#define MCU_IPW_IRQ_SW_MAJOR_VERSION                1
#define MCU_IPW_IRQ_SW_MINOR_VERSION                0
#define MCU_IPW_IRQ_SW_PATCH_VERSION                1


/*==================================================================================================
                                     FUNCTION PROTOTYPES
==================================================================================================*/
#define MCU_START_SEC_CODE
#include "Mcu_MemMap.h"

#if (MCU_CMU_PMC_SCG_INTERRUPT == STD_ON)
ISR( Mcu_PMC_SCG_CMU_Isr );
#endif

#define MCU_STOP_SEC_CODE
#include "Mcu_MemMap.h"

#ifdef __cplusplus
}
#endif

#endif /* MCU_IPW_IRQ_H */

/** @} */

