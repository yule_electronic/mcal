/**
*   @file    Mcu_SMC_Types.h
*   @version 1.0.1
*
*   @brief   AUTOSAR Mcu - Exported data outside of the Mcu from IPV_SMC.
*   @details Public data types exported outside of the Mcu driver.
*
*   @addtogroup MCU
*   @{
*/
/*==================================================================================================
*   Project              : AUTOSAR 4.3 MCAL
*   Platform             : ARM
*   Peripheral           : MC
*   Dependencies         : none
*
*   Autosar Version      : 4.3.1
*   Autosar Revision     : ASR_REL_4_3_REV_0001
*   Autosar Conf.Variant :
*   SW Version           : 1.0.1
*   Build Version        : S32K14x_MCAL_1_0_1_RTM_ASR_REL_4_3_REV_0001_20190621
*
*   (c) Copyright 2006-2016 Freescale Semiconductor, Inc. 
*       Copyright 2017-2019 NXP
*   All Rights Reserved.
==================================================================================================*/
/*==================================================================================================
==================================================================================================*/


#ifndef MCU_SMC_TYPES_H
#define MCU_SMC_TYPES_H


#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
                                         INCLUDE FILES
 1) system and project includes
 2) needed interfaces from external units
 3) internal and external interfaces from this unit
==================================================================================================*/
#include "Mcu_Cfg.h"
#include "Soc_Ips.h"

/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/**
* @brief            Power Modes encoding.
* @details          Supported power modes for SMC hw IP.
*/
typedef enum
{
    MCU_RUN_MODE    = 0x00U,   /**< @brief Run Mode. */
    MCU_HSRUN_MODE  = 0x01U,   /**< @brief High Speed Mode. */
    MCU_VLPR_MODE   = 0x02U,   /**< @brief Very Low Power Run Mode. */
    MCU_VLPS_MODE   = 0x03U,   /**< @brief Very Low Power Stop Mode. */
    MCU_STOP1_MODE  = 0x04U,   /**< @brief Stop 1 Mode. */
    MCU_STOP2_MODE  = 0x05U,   /**< @brief Stop 2 Mode. */
} Mcu_PowerModeType;

/**
* @brief   SMC IP configuration.
* @details This structure contains information for allowed modes
*/
typedef struct
{
   /* Allowed modes */
    VAR (uint32, MCU_VAR)  u32AllowedModes;
} Mcu_SMC_ConfigType;


/**
* @brief            Definition of a MCU mode section in the configuration structure.
* @details          Specifies the system behaviour during the selected target mode.
*                   Data set and configured by Mcu_SetMode call.
*/
typedef struct
{
    /**< @brief The ID for Power Mode configuration. */
    VAR( Mcu_ModeType, MCU_VAR) Mcu_ModeConfigId;
   /**< @brief Power modes control configuration */
    VAR (Mcu_PowerModeType, MCU_VAR)  u32PowerMode;
} Mcu_ModeConfigType;


#ifdef __cplusplus
}
#endif

#endif /* MCU_SMC_TYPES_H */

/** @} */
