
#include "S32K146.h"    /* include peripheral declarations S32K144 */
#include "Mcu.h"
#include "Port.h"
#include "Dio.h"
#include "Can.h"
#include "Lin.h"

#include "flexcan_driver.h"
#include "lpTmr1.h"


unsigned int timer=0;
unsigned int cycles = 2000000;
Mcu_ClockType ClockSetting;

/*****************************************************************************************
* used for lin debug
*****************************************************************************************/
unsigned int canSendStatus=0;
unsigned int canReceiveStatus=0;
unsigned char mb_data[8] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
unsigned char rx_data[8];
unsigned int rx_id=0;
flexcan_msgbuff_t rx_buffer;

/*****************************************************************************************
* used for lin debug
*****************************************************************************************/
#define DATA_SIZE                     (8U)
#define FRAME_SLAVE_RECEIVE_DATA      (1U)
#define FRAME_MASTER_RECEIVE_DATA     (2U)
#define FRAME_GO_TO_SLEEP             (3U)

/* Define for DATA buffer transmit */
uint8 txBuff[DATA_SIZE] = {0x18, 0x17, 0x16, 0x15, 0x14, 0x13, 0x12, 0x11};//used for slave node send data
/* Define for DATA buffer receiver */
uint8 rxBuff[DATA_SIZE] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

status_t linSendStatus=0;
status_t linReceiveStatus=0;


/**********************************************************************************
 * Function Prototype Define
 *********************************************************************************/

/**
* Func:     CallbackHandler()
* Desc:     Declare Callback handler function
*/
lin_callback_t CallbackHandler(uint32 instance, lin_state_t * lin1_State)
{
    lin_callback_t callbackCurrent;
    callbackCurrent = lin1_State->Callback;

    switch (lin1_State->currentEventId)
    {
        case LIN_PID_OK:
            /* Set timeout */
            //LIN_DRV_SetTimeoutCounter(INST_LIN0, TIMEOUT);

            /* If PID is 0x01, salve node will receive data from master node */
            if(FRAME_SLAVE_RECEIVE_DATA == lin1_State->currentId)
            {
                /* Call to Receive Frame DATA Function */
                linReceiveStatus = LIN_DRV_ReceiveFrameData(INST_LIN1, rxBuff, sizeof(rxBuff));
            }

            /* If PID is 0x02, salve node will send data */
            if(FRAME_MASTER_RECEIVE_DATA == lin1_State->currentId)
            {
                /* Call to Send Frame DATA Function */
                linSendStatus = LIN_DRV_SendFrameData(INST_LIN1, txBuff, sizeof(txBuff));
            }

            /* If PID is 0x03, salve node will go to sleep mode */
            if(FRAME_GO_TO_SLEEP == lin1_State->currentId)
            {
                /* Go to sleep mode */
                LIN_DRV_GoToSleepMode(INST_LIN1);
            }
            break;
        case LIN_PID_ERROR:
            /* Go to idle mode */
            LIN_DRV_GoToSleepMode(INST_LIN1);
            break;
        case LIN_TX_COMPLETED:
        case LIN_RX_COMPLETED:
            /* Go to idle mode */
            LIN_DRV_GotoIdleState(INST_LIN1);
            break;
        case LIN_CHECKSUM_ERROR:
        case LIN_READBACK_ERROR:
        case LIN_FRAME_ERROR:
        case LIN_RECV_BREAK_FIELD_OK:
            /* Set timeout */
            //LIN_DRV_SetTimeoutCounter(INST_LIN0, TIMEOUT);
            break;
        case LIN_WAKEUP_SIGNAL:
            /* Set wakeup signal flag */
            //wakeupSignalFlag = true;
            break;
        case LIN_SYNC_ERROR:
        case LIN_BAUDRATE_ADJUSTED:
        case LIN_NO_EVENT:
        case LIN_SYNC_OK:
        default:
        /* do nothing */
            break;
    }

    return callbackCurrent;
}



    
int main(void)
{
    uint32 i=0;
    
    /**************************************************************************************
     * Mcu initial
     *************************************************************************************/
    Mcu_InitClock( ClockSetting );

    
    /**************************************************************************************
     * Port initial
     *************************************************************************************/
    Port_Init( (void*)0 );
    Port_SetPinDirection(GPIO_LED, PORT_PIN_OUT);
    
    
    /**************************************************************************************
     * Can initial
     *************************************************************************************/
    Can_Init( (void*)0 );//replaced by autosar api
    

    /**************************************************************************************
     * Lin initial
     *************************************************************************************/
    /* Set LIN transceiver initial */
    Port_SetPinDirection(GPIO_LIN_EN, PORT_PIN_OUT);
    Dio_WriteChannel(GPIO_LIN_EN, 1);
    Lin_Init( (void*)0 );
    
    
    
    
    for(;;)
    {
        while(cycles--);
        cycles =2000000;

        timer++;
        Dio_WriteChannel(GPIO_LED, (timer%2));

        //ld_send_message(LI0, 4, 0x02, linTxBuffer);
        
        canSendStatus    = FLEXCAN_DRV_Send(INST_CANCOM, 0, &tx_info, 0x111, mb_data);
    	canReceiveStatus = FLEXCAN_DRV_Receive(INST_CANCOM, 1, &rx_buffer);

    	if(STATUS_SUCCESS == canReceiveStatus)
    	{
            rx_id = rx_buffer.msgId;
            for(i=0; i<rx_buffer.dataLen; i++)
            {
                rx_data[i] = rx_buffer.data[i];
            }
    	}
    }
    
}
